<!DOCTYPE html>


<html >
    <head>
       <title>Encyclopedia listing</title>
       <meta charset="UTF-8">
       <meta name="description" content="Encyclopedias listing">
       <meta name="keywords" content="DB, Encyclosphere">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="noindex,nofollow">
       <link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../favicon/favicon.ico"/>
       <meta name="author" content="KSF (S.Chekanov)">
       <link rel="stylesheet" type="text/css" href="css/datatables.css"/>
     	<style type="text/css" title="currentStyle">


                 @import "css/demo.css";

		</style>
		<script type="text/javascript" language="javascript" src="js/jquery-1.12.4.min.js"></script>
                <script type="text/javascript" src="js/datatables.min.js"></script>


		<!-- Start here -->
		<?php 
		 include "serverdatapdo.php";   // this class handles is both server and client side data
		 
		 //now generate the datatables Jquery with this SQL here:
		 //for added security move this array into to the serverside client
		 $db_array=array( 
				"sql"=>'SELECT Id,Name,Domain,Website,Categories,Count,License,Timestamp,EncyclopshereCandidate,Lang,HashID FROM files', /* Spell out columns names no SELECT * Table */
				"table"=>'encyclopedias', /* DB table to use assigned by constructor*/
				"idxcol"=>'Id' /* Indexed column (used for fast and accurate table cardinality) */
					);
		$javascript = ServerDataPDO::build_jquery_datatable($db_array);
		echo $javascript;
		?>		


    </head>
    <body>

    <div class="container">
    <h2>Encyclopedia listing</h2>
     Here is the list of encyclopedias from the KSF database.
     Click the column header for sorting. 
     <p></p>

<div style="float:left; margin-bottom:20px;font-size:1.3em; margin-top:10px;">    
<a href="new.php">Suggest a new encyclopedia</a>
</div>


 <?php
                 //print_r($ini_array);
                $cols="Id,Name,Domain,Language,Categories,Nr,License,Time,IsIn?,Info";  //Column names for datatable headings (typically same as sql)
		$html =ServerDataPDO::build_html_datatable($cols);
		echo  $html;
		?>

    </div>



<div class="footer">
</br>
<a href='../'>EncycloReader</a>
supported by the <a href='https://encyclosphere.org/about/'><img src='../img/150px-Encyclosphere_logo_image_alone_24.png' alt="Encyclosphere" style='vertical-align:middle;margin:0;'/>KSF</a>
</div>



    </body>
</html>
