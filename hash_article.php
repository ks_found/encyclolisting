<?php

/**
 * Create a short, fairly unique, urlsafe hash for the input string.
 */
function generate_id( $input, $length = 12 ){
        // Create a raw binary sha256 hash and base64 encode it.
        $hash_base64 = base64_encode( hash( 'sha256', $input, true ) );
        // Replace non-urlsafe chars to make the string urlsafe.
        $hash_urlsafe = strtr( $hash_base64, '+/-_', '0abc' );
        // Trim base64 padding characters from the end.
        $hash_urlsafe = rtrim( $hash_urlsafe, '=' );
        // Shorten the string before returning.
	return substr( $hash_urlsafe, 0, $length );
}


//$ss=generate_id("s0andbox/sandbox.org/glacial-man-in-ohio1");
//echo $ss;
?>

