<?php


$id="0";
if (isset($_GET['id'])) $id = $_GET['id'];
if (strlen($id) != 5) die("ID of s not found");

require_once("common.php");


$strHEADER = <<<EOD
<meta charset="UTF-8">
<link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
<link rel="manifest" href="../favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="../favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="View encyclopedia entry" />
        <meta name="keywords" content="encyclopedias, encyclosphere, edutech, wikia, wikipedia, citizendium, handwiki" />
        <meta name="author" content="KSF" />
        <title>View article</title>

        <link rel="stylesheet" type="text/css" href="../css/autocom.css"/>
        <link rel="stylesheet" type="text/css" href="../css/hwsimple.css"/>
        <link rel="stylesheet" type="text/css" href="../css/mobile.css"/>
        <link rel="stylesheet" type="text/css" href="../css/footer.css"/>
        <link rel="stylesheet" type="text/css" href="../css/showencyclopedias.css"/>


<style>
#ksf {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#ksf td, #ksf th {
  border: 1px solid #ddd;
  padding: 8px;
}

#ksf tr:nth-child(even){background-color: #f2f2f2;}

#ksf tr:hover {background-color: #ddd;}

#ksf th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #133383;
  color: white;
}
</style>

</body>

<div class="container">
EOD;



$strFooter = <<<EOD

</div>


<div class="footer">
</br>
<a href='https://encyclosphere.org/listing/'>Encyclopedia directory listing</a>
by the <a href='https://encyclosphere.org/about/'><img src='../img/150px-Encyclosphere_logo_image_alone_24.png' alt="Encyclosphere" style='vertical-align:middle;margin:0;'/>KSF</a>
</div>

</body>
</html>

EOD;



$db = null;
try {
   $db = new PDO($databasefile);
} catch( PDOException $e ) {
   die( $e->getMessage() );}


$sql = "SELECT * FROM " . $sTable . " WHERE HashID='$id'" . " LIMIT 1;";


if ($sth = $db->prepare($sql)) {
           $sth->execute();
}

# echo $sql . " from " . $databasefile;

$rowarray = $sth->fetchall(PDO::FETCH_ASSOC);
$rowno = 0;

$Name="";
$Domain="";
$Website="";
$Lang="";
$Categories="";
$Keywords="";
$Count="";
$ZWICount="";
$License="";
$LicenseURL="";
$Access="";
$Status="";
$ForkOf="";
$Description=""; 
$Notes="";
$Collection="";
$IsInEncyclopshere="";
$Timestamp="";
$UserName="None";
$FullName="None";
$Email="None";

foreach($rowarray as $row) {

if (isset($row['Name']))
        $Name=$row['Name'];

if (isset($row['Domain']))
        $Domain=$row['Domain'];

if (isset($row['Website']))
        $Website=$row['Website'];

if (isset($row['Lang']))
        $Lang=$row['Lang'];

if (isset($row['Categories']))
        $Categories=$row['Categories'];

if (isset($row['License']))
        $License=$row['License'];

if (isset($row['LicenseURL']))
        $LicenseURL=$row['LicenseURL'];

if (isset($row['Keywords']))
        $Keywords=$row['Keywords'];

if (isset($row['Count']))
        $Count=$row['Count'];

if (isset($row['ZWICount']))
        $ZWICount=$row['ZWICount'];

if (isset($row['Access']))
        $Access=$row['Access'];

if (isset($row['Status']))
        $Status=$row['Status'];

if (isset($row['ForkOf']))
        $ForkOf=$row['ForkOf'];

if (isset($row['Description']))
        $Description=$row['Description'];

if (isset($row['Notes']))
        $Notes=$row['Notes'];

if (isset($row['Collection']))
        $Collection=$row['Collection'];

if (isset($row['EncyclopshereCandidate']))
        $IsInEncyclopshere=$row['EncyclopshereCandidate'];

if (isset($row['Timestamp'])) { 
        $Timestamp=$row['Timestamp'];
        $Timestamp= date('m/d/Y H:i:s', $Timestamp);
        };

if (isset($row['UserName']))
        if (strlen($row['UserName'])>1)  $UserName=$row['UserName'];

if (isset($row['FullName']))
        if (strlen($row['FullName'])>1)  $FullName=$row['FullName'];

if (isset($row['Email']))
        if (strlen($row['Email'])>1)  $Email=$row['Email'];


}



print($strHEADER);

$web="<a href=\"".$Website."\" target=\"_blank\">" . $Website  . "</a>";

echo "<table  id=\"ksf\">";
echo "<tr><th>Attribute</th><th>Value</th></tr>";
echo "<tr><td>Name</td><td>" . $Name  . "</td></tr>";
echo "<tr><td>Webpage</td><td>" . $web  . "</td></tr>";
echo "<tr><td>Domain</td><td>" . $Domain  . "</td></tr>";
echo "<tr><td>Language</td><td>" . $Lang  . "</td></tr>";
echo "<tr><td>Categories</td><td>" . $Categories  . "</td></tr>";
echo "<tr><td>Keywords</td><td>" . $Keywords  . "</td></tr>";
echo "<tr><td>License</td><td>" . $License  . " URL:" . $LicenseURL . "</td></tr>";
echo "<tr><td>Nr of articles</td><td>" . $Count  . "</td></tr>";
echo "<tr><td>Nr of ZWI files</td><td>" . $ZWICount  . "</td></tr>";
echo "<tr><td>Access</td><td>" . $Access  . "</td></tr>";
echo "<tr><td>Status</td><td>" . $Status  . "</td></tr>";
echo "<tr><td>ForkOf</td><td>" . $ForkOf  . "</td></tr>";
echo "<tr><td>Description</td><td>" . $Description  . "</td></tr>";
echo "<tr><td>Notes</td><td>" . $Notes  . "</td></tr>";
echo "<tr><td>Collection</td><td>" . $Collection  . "</td></tr>";
echo "<tr><td>Encyclosphere candidate</td><td>" . $IsInEncyclopshere  . "</td></tr>";
echo "<tr><td>Modified by</td><td>" . $FullName  . " (" . $UserName . ") Email:".  $Email ."</td></tr>";
echo "<tr><td>Modification time</td><td>" . $Timestamp  . "</td></tr>";

echo "</table>";


print($strFooter); 

$db = null;


?>



