<!DOCTYPE html>

<html >
    <head>
       <title>New encyclopedia</title>
       <meta charset="UTF-8">
       <meta name="description" content="Encyclopedias listing">
       <meta name="keywords" content="DB, Encyclosphere">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="robots" content="noindex,nofollow">
       <link REL="SHORTCUT ICON" HREF="../favicon/favicon.ico">
       <link rel="apple-touch-icon" href="../favicon/favicon.ico"/>
       <meta name="author" content="KSF (S.Chekanov)">
       <link rel="stylesheet" type="text/css" href="css/demo.css"/>

<style>
* {
  box-sizing: border-box;
  font-size: 1em;
}

h1 {
  font-size: 2.5em;
}

h2 {
  font-size: 1.8em;
}


input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #372989;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #297889;
}

.container {
  font-size: 1em;
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

textarea  
{  
   font-family:Arial,"Times New Roman",Times, serif;  
   font-size: 1em;   
}


/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>

    </head>
    <body>


 <div class="container">

  <h2>Suggest a new encyclopedia</h2>
 
  <form action="push.php" method="get">

    <div class="row">
      <div class="col-25">
        <label for="title">Encyclopedia's name:</label>
      </div>
      <div class="col-75">
        <input type="text" id="title" name="title" placeholder="Full name of the encyclopedia">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="url">URL:</label>
      </div>
      <div class="col-75">
        <input type="text" id="url" name="url" placeholder="URL starting with http">
      </div>
    </div>


    <div class="row">
      <div class="col-25">
        <label for="license">License</label>
      </div>
      <div class="col-75">
        <select id="license" name="license">
          <option value="CC BY-SA">CC BY-SA</option>
          <option value="BY-SA">BY-SA</option>
          <option value="BY-NC">BY-NC</option>
          <option value="Commercial">Commercial</option>
	</select>
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="URLlicense">URL of the License:</label>
      </div>
      <div class="col-75">
        <input type="text" id="URLlicense" name="URLlicense" placeholder="URL of this license ..">
      </div>
    </div>


    <div class="row">
      <div class="col-25">
        <label for="lang">Language:</label>
      </div>
      <div class="col-75">
        <input type="text" id="lang" name="lang" placeholder="Language(s) of this encyclopedia">
      </div>
    </div>


   <div class="row">
      <div class="col-25">
        <label for="comment">Comment</label>
      </div>
      <div class="col-75">
        <textarea id="comment" name="comment" placeholder="Any comment for the KSF reviewers .. " style="height:100px;"></textarea>
      </div>
    </div>



    <div class="row">
      <div class="col-25">
        <label for="check">Type security code:</label>
      </div>
      <div class="col-75">

<div style="float:left;">
     <input name="check" maxlength="3" size="3"  type="text" id="check" />
     <img src="captchas.php" ALIGN=ABSMIDDLE>
</div>

      </div>
    </div>




    <div class="row">
      <input type="submit" value="Submit">
    </div>
  </form>

</div>

<div style="float:left; margin-bottom:20px;font-size:1.1em; margin-top:10px;">
Note: your entry will be reviewed by the KSF experts before it will be included to the KSF encyclosphere.
</div>

<p>
</p>

<div class="footer">
</br>
<a href='../'>EncycloReader</a>
supported by the <a href='https://encyclosphere.org/about/'><img src='../img/150px-Encyclosphere_logo_image_alone_24.png' alt="Encyclosphere" style='vertical-align:middle;margin:0;'/>KSF</a>
</div>



</body>
</html>
