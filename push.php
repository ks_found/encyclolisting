<?php
// get the data and push to database
// S.Chekanov
$TITLE=trim($_GET["title"]);
$URL=trim($_GET["url"]);
$License=trim($_GET["license"]);
$URLlicense=trim($_GET["URLlicense"]);
$Notes=trim($_GET["comment"]);
$Lang=trim($_GET["lang"]);

if (strlen($TITLE)<2 or strlen($TITLE)>80)
          die("The title is too short or long");

if (strlen($URL)<2 or strlen($URL)>80)
          die("The URL is too short or long");

if(filter_var($URL, FILTER_VALIDATE_URL) == false)
           die("URL is wrong");

if (strlen($URLlicense)>1)
  if(filter_var($URLlicense, FILTER_VALIDATE_URL) == false)
           die("URL of the license is wrong");

$SEC=trim($_GET['check']);

if(strlen( $SEC )>0) { 
                        // Open the file for security code captchas
                        $fp = fopen("tmp/captchas.txt", "r");
                        $count = fread($fp, 1024);
                        fclose($fp);
                        if($SEC != $count) {
                            die(" Wrong security code. Try again.");
                         }

}

// SQL database
require_once("./common.php");
require_once("./hash_article.php");


$db = null;
try {
   $db = new PDO($databasefile);
   $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

} catch( PDOException $e ) {
   die( $e->getMessage() );}

$URL=rtrim($URL, "/");
$HashID=generate_id( $URL, 5 );

$CURRENT_TIME=time();

try{
 $stmt = $db->prepare("INSERT INTO submissions (Title,URL,LicenseURL,License,Notes,Lang,Timestamp,HashID) VALUES (?,?,?,?,?,?,?,?)");
 $stmt->bindParam(1, $TITLE);
 $stmt->bindParam(2, $URL);
 $stmt->bindParam(3, $URLlicense);
 $stmt->bindParam(4, $License);
 $stmt->bindParam(5, $Notes);
 $stmt->bindParam(6, $Lang);
 $stmt->bindParam(7, $CURRENT_TIME);
 $stmt->bindParam(8, $HashID);
 $stmt->execute();

} catch(PDOException $e) {print "Error while inserting..<br>"; echo $e->getMessage();}

$db = null;

$str = <<<EOD
<!DOCTYPE html>
Name of encyclopedia: $TITLE  </br>
URL of encyclopedia:  $URL </br>
Language:  $Lang </br>
License: $License </br>
HashID: $HashID </br>
<b>Submitted for review by the KSF. Thanks! </b>
<p>
</p>
<button onclick="window.location.href='index.php';">
     Go back to the listing 
</button>

</html>
EOD;

echo $str;

?> 

