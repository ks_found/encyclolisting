<?php

/**
* @author Sergei V Chekanov <schekanov@jwork.org>
*/

        $databasefile="sqlite:manage/encyclopedias.sqlite";
        $aColumns = array( 'Id', 'Name', 'Domain', 'Website', 'Lang', 'Categories', 'Keywords', 'Count', 'ZWICount', 'License', 'LicenseURL', 'Access', 'Status',  'ForkOf',  'Description', 'Notes', 'Collection',  'EncyclopshereCandidate', 'Timestamp', 'HashID');
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "Id";
        /* DB table to use */
        $sTable = "encyclopedias";


// limit text size
function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
 return $text;
}



?>

